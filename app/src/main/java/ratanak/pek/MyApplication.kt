package ratanak.pek


import android.app.Application
import android.content.res.Configuration
import android.os.Build
import android.preference.PreferenceManager
import java.util.*


class MyApplication : Application() {
    private var locale: Locale? = null
    override fun onCreate() {
        super.onCreate()
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val config: Configuration = baseContext.resources.configuration
        val lang = preferences.getString("km-rKH", "km-rKH")
        val systemLocale: String =
            getSystemLocale(config).language
        if ("" != lang && systemLocale != lang) {
            locale = Locale(lang)
            Locale.setDefault(locale)
            setSystemLocale(config, locale)
            updateConfiguration(config)
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        if (locale != null) {
            setSystemLocale(newConfig!!, locale)
            Locale.setDefault(locale)
            updateConfiguration(newConfig)
        }
    }

    private fun updateConfiguration(config: Configuration) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            baseContext.createConfigurationContext(config)
        } else {
            baseContext.resources
                .updateConfiguration(config, baseContext.resources.displayMetrics)
        }
    }

    companion object {
        private fun getSystemLocale(config: Configuration): Locale {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                config.locales.get(0)
            } else {
                config.locale
            }
        }

        private fun setSystemLocale(config: Configuration, locale: Locale?) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                config.setLocale(locale)
            } else {
                config.locale = locale
            }
        }
    }
}
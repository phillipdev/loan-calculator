package ratanak.pek

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_loan_data.*

import ratanak.pek.data.LoanDataModel
import ratanak.pek.util.LoanAdapter
import java.lang.reflect.Type


class LoanDataActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_data)


        val loanData = intent.getStringExtra("rtk_obj")

        if (loanData.isNotEmpty()) {
            val founderListType: Type = object : TypeToken<ArrayList<LoanDataModel?>?>() {}.type
            val selectedResultSearch: ArrayList<LoanDataModel?> =
                Gson().fromJson(loanData, founderListType)

            rcLoan.layoutManager = LinearLayoutManager(this)
            rcLoan.adapter = LoanAdapter(selectedResultSearch, this)
        }

        val monthTotal = intent.getStringExtra("rtk_month")
        if (monthTotal.isNotEmpty()) {
            tvMonthlyTotal.text = "$monthTotal $"
        }

        val rateTotal = intent.getStringExtra("rtk_rate")
        if (rateTotal.isNotEmpty()) {
            tvRateTotal.text = "$rateTotal $"
        }
        val month = intent.getStringExtra("rtk_month_edt")
        val rate = intent.getStringExtra("rtk_rate_edt")
        val amount = intent.getStringExtra("rtk_amount_edt")
       if(month.isNotEmpty() && rate.isNotEmpty() && amount.isNotEmpty()){
          tvTitleBorrow.text = resources.getString(R.string.borrow) +" "+amount +"$ " +resources.getString(R.string.with_rate) +" "+rate+"%, "+ resources.getString(R.string.pay_in)+" " +month + " "+resources.getString(R.string.months)
       }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)



    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId== android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}

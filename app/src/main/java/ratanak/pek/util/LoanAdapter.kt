package ratanak.pek.util


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.loan_data_item.view.*
import ratanak.pek.R
import ratanak.pek.data.LoanDataModel
import kotlin.collections.ArrayList

class LoanAdapter(val items: ArrayList<LoanDataModel?>, val context: Context) :
    RecyclerView.Adapter<ViewHolder>() {

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.loan_data_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.tvMonthNumber?.text = items.get(position)?.numberOfMonth
        holder?.tvMonthlyPay?.text = items.get(position)?.monthlyPayment
        holder?.tvOriginalAmount?.text = items.get(position)?.originalAmount
        holder?.tvRatePayment?.text = items.get(position)?.interestAmount
        holder?.tvRemainPayment?.text = items.get(position)?.remainAmount
    }

}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val tvMonthNumber = view.tvMonthNumber
    val tvMonthlyPay = view.tvMonthlyPay
    val tvOriginalAmount = view.tvOriginalAmount
    val tvRatePayment = view.tvRatePayment
    val tvRemainPayment = view.tvRemainPayment

}
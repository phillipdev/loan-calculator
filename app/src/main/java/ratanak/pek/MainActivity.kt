package ratanak.pek



import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import ratanak.pek.data.LoanDataModel
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    var loanType = 0
    var totalAmountToPay = 0.0
    var totalInterest = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        rdGroupTypeLoan.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.decreasePayment) {
                loanType = 0
                decreasePayment.isChecked = true
                fixedPayment.isChecked = false
            } else if (checkedId == R.id.fixedPayment) {
                loanType = 1
                decreasePayment.isChecked = false
                fixedPayment.isChecked = true

            }
        }


        btnCalculate.setOnClickListener {
            if (validate()) {
                //calculation
                //show loading
                val format = DecimalFormat("0.#")

                var loanObj =
                    if (loanType == 0) findNumberOfMonthToPay(
                        edtMonth.text.toString().toInt(),
                        edtRate.text.toString().toDouble(),
                        edtAmount.text.toString().toDouble()
                    ) else findNumberOfMonthToPayFixPayment(
                        edtMonth.text.toString().toInt(),
                        edtRate.text.toString().toDouble(),
                        edtAmount.text.toString().toDouble()
                    )

                val intentData = Intent(this@MainActivity, LoanDataActivity::class.java)
                val gson = Gson().toJson(loanObj);
                intentData.putExtra("rtk_obj", gson)
                intentData.putExtra("rtk_month", roundOff(2, totalAmountToPay))
                intentData.putExtra("rtk_rate", roundOff(2, totalInterest))

                intentData.putExtra("rtk_amount_edt", edtAmount.text.toString())
                intentData.putExtra("rtk_rate_edt", edtRate.text.toString())
                intentData.putExtra("rtk_month_edt",  edtMonth.text.toString())


                //reset
                totalAmountToPay = 0.0
                totalInterest = 0.0

                startActivity(intentData)

                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            }
        }


    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        val inflater = menuInflater
//         inflater.inflate(R.menu.setting_menu, menu)
//        return super.onCreateOptionsMenu(menu)
//    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.khmer -> {
                Toast.makeText(this, "Khmer", Toast.LENGTH_LONG).show()
            }
            R.id.english -> {
                Toast.makeText(this, "English", Toast.LENGTH_LONG).show()
            }

        }
        return true
    }

    fun validate(): Boolean {

        var isValid = true

        if (edtAmount.text!!.isEmpty() || edtAmount.text.toString().toDouble()<1) {
            textInputAmount.error = resources.getString(R.string.amount_can_not_be_empty)
            isValid = false
        } else {
            textInputAmount.error = null
        }

        if (edtMonth.text!!.isEmpty() || edtMonth.text!!.toString().toDouble()<1) {
            textInputMonth.error = resources.getString(R.string.month_can_not_be_empty)
            isValid = false
        } else {
            textInputMonth.error = null
        }

        if (edtRate.text!!.isEmpty() || edtRate.text!!.toString().toDouble()<=0) {
            textInputRate.error = resources.getString(R.string.rate_can_not_be_empty)
            isValid = false
        } else {
            textInputRate.error = null
        }

        return isValid
    }

    private fun findNumberOfMonthToPay(
        month: Int,
        rate: Double,
        amountOfLoan: Double
    ): ArrayList<LoanDataModel> {
        var amountLocal = amountOfLoan
        val loanList = ArrayList<LoanDataModel>()

        for (item in 1 until month + 1) {
            val monthlyRemain = amountLocal - findMonthlyPayment(month, amountOfLoan)
            val interest = (amountLocal * rate) / 100;
            val loan = LoanDataModel()

            loan.numberOfMonth = "$item"
            loan.monthlyPayment = roundOff(2, findMonthlyPayment(month, amountOfLoan) + interest)
            loan.originalAmount = roundOff(2, findMonthlyPayment(month, amountOfLoan))
            loan.interestAmount = roundOff(2, interest)
            loan.remainAmount = roundOff(2, monthlyRemain)

            totalAmountToPay += findMonthlyPayment(month, amountOfLoan) + interest

            totalInterest += interest

            amountLocal = monthlyRemain

            loanList.add(loan)
        }
        return loanList
    }

    private fun findMonthlyPayment(month: Int, amountOfLoan: Double): Double {
        return amountOfLoan / month;
    }

    private fun roundOff(scale: Int, input: Double): String? {
        val mInput = input.toString()
        var result = ""
        if (mInput.contains(".")) {
            val split = mInput.split(".").toTypedArray() // split by '.'
            val splitter = split[1]
            result = try {
                val c = splitter[scale] // find the character to compare
                val value = c.toString().toInt()
                if (value >= 5) {
                    BigDecimal.valueOf(input).setScale(scale, RoundingMode.UP).stripTrailingZeros()
                        .toPlainString()
                } else {
                    BigDecimal.valueOf(input).setScale(scale, RoundingMode.DOWN)
                        .stripTrailingZeros().toPlainString()
                }
            } catch (e: StringIndexOutOfBoundsException) {
                // MSFLog.warning(Log.getStackTraceString(e))
                mInput
            }
        }
        return thousandSeparatorWithTrailingZero(result)
    }

    private fun thousandSeparatorWithTrailingZero(input: String): String? {
        val nf: NumberFormat = NumberFormat.getNumberInstance(Locale.US)
        val decimalFormat: DecimalFormat = nf as DecimalFormat
        decimalFormat.applyPattern("###,###.#")
        decimalFormat.maximumFractionDigits = 15
        var bigDecimal: BigDecimal = when {
            input.contains(",") -> {
                BigDecimal(input.replace(",", ""))
            }
            input.contains("-") -> {
                return input
            }
            else -> {
                BigDecimal(input)
            }
        }
        bigDecimal = bigDecimal.setScale(15, RoundingMode.DOWN).stripTrailingZeros()
        return decimalFormat.format(bigDecimal)
    }


    private fun findNumberOfMonthToPayFixPayment(
        month: Int,
        rate: Double,
        amountOfLoan: Double
    ): ArrayList<LoanDataModel> {
        var amountLocal = amountOfLoan
        val loanList = ArrayList<LoanDataModel>()

        for (item in 1 until month + 1) {

            val interest = (amountLocal * getRatePercent(rate));

            val romlous = if ((findMonthlyPaymentByformula(
                    month,
                    amountOfLoan,
                    rate
                ) - interest) > amountLocal
            ) amountLocal else (findMonthlyPaymentByformula(month, amountOfLoan, rate) - interest)

            val monthlyRemain = amountLocal - romlous

            val loan = LoanDataModel()
            loan.numberOfMonth = "$item"
            loan.monthlyPayment =
                roundOff(2, findMonthlyPaymentByformula(month, amountOfLoan, rate))
            loan.originalAmount = roundOff(2, romlous)
            loan.interestAmount = roundOff(2, interest)
            loan.remainAmount = roundOff(2, monthlyRemain)

            totalAmountToPay += findMonthlyPayment(month, amountOfLoan) + interest

            totalInterest += interest

            //  println("Month : $item " + " -> monthly pay: " +roundOff(2,findMonthlyPaymentByformula()) + "$ -> interest : "+ roundOff(2, interest) +"$ -> Pay :" +roundOff(2, romlous) +"$ -> Remain monthly:" + roundOff(2, monthlyRemain)+"$")
            amountLocal = monthlyRemain

            loanList.add(loan)
        }
        return loanList
    }

    private fun getRatePercent(rate: Double): Double {
        return rate / 100.0
    }

    private fun findMonthlyPaymentByformula(
        month: Int,
        amountOfLoan: Double,
        rate: Double
    ): Double {
        return (amountOfLoan * getRatePercent(rate)) / (1 - Math.pow(
            1 + getRatePercent(rate),
            -month.toDouble()
        ))
    }


}

package ratanak.pek.data

class LoanDataModel {
    var numberOfMonth: String? = null
    var monthlyPayment: String? = null
    var originalAmount: String? = null
    var interestAmount: String? = null
    var remainAmount: String? = null

}
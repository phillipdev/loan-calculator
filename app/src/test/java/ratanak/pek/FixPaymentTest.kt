package ratanak.pek

import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class FixPaymentTest {

    val month = 24;

    val rate = 1;

    var amountOfLoan = 5000.0;


    @Test
    fun findNumberOfMonthToPay() {


       // println(findMonthlyPaymentByformula())
        var amountLocal = amountOfLoan
        for (item in 1 until month + 1) {

            var interest = (amountLocal*getRatePercent());

            var romlous = if((findMonthlyPaymentByformula()-interest)>amountLocal) amountLocal else (findMonthlyPaymentByformula()-interest)

            var monthlyRemain = amountLocal - romlous

            println("Month : $item " + " -> monthly pay: " +roundOff(2,findMonthlyPaymentByformula()) + "$ -> interest : "+ roundOff(2, interest) +"$ -> Pay :" +roundOff(2, romlous) +"$ -> Remain monthly:" + roundOff(2, monthlyRemain)+"$")
            amountLocal = monthlyRemain
        }
    }

    //    @Test
    fun findMonthlyPayment(): Double {
        return amountOfLoan / month;
    }

    @Test
    fun findInterestMonthly(){
        var totalInterest=0.0;
        var amountLocal = amountOfLoan
        for (item in 1 until month + 1) {
           var firstInterst = (amountLocal * getRatePercent())/100
            amountLocal-=firstInterst
            println(amountLocal)

        }

    }

    fun getRatePercent():Double{
        return rate/100.0
    }


    fun findMonthlyPaymentByformula():Double{
        return (amountOfLoan*getRatePercent())/(1-Math.pow(1+getRatePercent(), -month.toDouble()))

    }

    @Test
    fun fasdfsdf() {
        println(roundOff(2, 5000.0555345))
    }

    fun roundOff(scale: Int, input: Double): String? {
        val mInput = input.toString()
        var result: String? = ""
        if (mInput.contains(".")) {
            val split = mInput.split(".").toTypedArray() // split by '.'
            val splitter = split[1]
            result = try {
                val c = splitter[scale] // find the character to compare
                val value = c.toString().toInt()
                if (value >= 5) {
                    BigDecimal.valueOf(input).setScale(scale, RoundingMode.UP).stripTrailingZeros().toPlainString()
                } else {
                    BigDecimal.valueOf(input).setScale(scale, RoundingMode.DOWN).stripTrailingZeros().toPlainString()
                }
            } catch (e: StringIndexOutOfBoundsException) {
               // MSFLog.warning(Log.getStackTraceString(e))
                mInput
            }
        }
        return result
    }

    @Test
    fun formatString(){
        val price = 0000.0
        val format = DecimalFormat("0.#")
        System.out.println(format.format(price))
    }

}

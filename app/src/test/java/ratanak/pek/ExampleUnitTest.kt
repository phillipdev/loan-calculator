package ratanak.pek

import android.util.Log
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    val month = 24;

    val rate = 0.1;

    var amountOfLoan = 3000.0;


    @Test
    fun findNumberOfMonthToPay() {
        var amountLocal = amountOfLoan
        for (item in 1 until month + 1) {
            var monthlyRemain = amountLocal - findMonthlyPayment()
            var interest = (amountLocal*rate)/100;
            println("Month : $item " + " -> monthly pay: " +roundOff(2, findMonthlyPayment()) + "$ -> interest : "+ roundOff(2, interest) +"$ -> Pay :" +roundOff(2, findMonthlyPayment()+interest) +"$ -> Remain monthly:" + roundOff(2, monthlyRemain)+"$")
            amountLocal = monthlyRemain
        }
    }

    //    @Test
    fun findMonthlyPayment(): Double {
        return amountOfLoan / month
    }


    fun findMonthlyIncomeInterest(): Double {
        return (amountOfLoan * rate) / 100

    }


    fun findAmountToPay(): Double {
        return findMonthlyPayment() + findMonthlyIncomeInterest()
    }

    fun findMonthlyRemain(): Double {
        return amountOfLoan - findMonthlyPayment()
    }


    @Test
    fun fasdfsdf() {
        println(roundOff(2, 5000.0555345))
    }

    fun roundOff(scale: Int, input: Double): String? {
        val mInput = input.toString()
        var result: String? = ""
        if (mInput.contains(".")) {
            val split = mInput.split(".").toTypedArray() // split by '.'
            val splitter = split[1]
            result = try {
                val c = splitter[scale] // find the character to compare
                val value = c.toString().toInt()
                if (value >= 5) {
                    BigDecimal.valueOf(input).setScale(scale, RoundingMode.UP).stripTrailingZeros().toPlainString()
                } else {
                    BigDecimal.valueOf(input).setScale(scale, RoundingMode.DOWN).stripTrailingZeros().toPlainString()
                }
            } catch (e: StringIndexOutOfBoundsException) {
               // MSFLog.warning(Log.getStackTraceString(e))
                mInput
            }
        }
        return result
    }

}
